import java.util.Scanner;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public int getDay() {
        return this.day;
    }
    public void setDay(int day) {
        if (day < 1 || day > 31) return;
        this.day = day;
    }
    public int getMonth() {
        return this.month;
    }
    public void setMonth(int month) {
        if (month < 1 || month > 12) return;
        this.month = month;
    }
    public int getYear() {
        return this.year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public MyDate() {
    }
    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public MyDate(String parameter){
        String[] words = parameter.split("\\s");
        if (words[0].equals("January")) this.month = 1;
        if (words[0].equals("February")) this.month = 2;
        if (words[0].equals("March")) this.month = 3;
        if (words[0].equals("April")) this.month = 4;
        if (words[0].equals("May")) this.month = 5;
        if (words[0].equals("June")) this.month = 6;
        if (words[0].equals("July")) this.month = 7;
        if (words[0].equals("August")) this.month = 8;
        if (words[0].equals("September")) this.month = 9;
        if (words[0].equals("October")) this.month = 10;
        if (words[0].equals("November")) this.month = 11;
        if (words[0].equals("December")) this.month = 12;
        if(words[1].length() == 4) words[1] = words[1].substring(0, 2);
        else words[1] = words[1].substring(0, 1);
        this.day = Integer.parseInt(words[1]);

        this.year = Integer.parseInt(words[2]);
    }
    public void accept(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a date(String): ");
        String parameter = keyboard.nextLine();
        String[] words = parameter.split("\\s");
        if (words[0].equals("January")) this.month = 1;
        if (words[0].equals("February")) this.month = 2;
        if (words[0].equals("March")) this.month = 3;
        if (words[0].equals("April")) this.month = 4;
        if (words[0].equals("May")) this.month = 5;
        if (words[0].equals("June")) this.month = 6;
        if (words[0].equals("July")) this.month = 7;
        if (words[0].equals("August")) this.month = 8;
        if (words[0].equals("September")) this.month = 9;
        if (words[0].equals("October")) this.month = 10;
        if (words[0].equals("November")) this.month = 11;
        if (words[0].equals("December")) this.month = 12;
        if(words[1].length() == 4) words[1] = words[1].substring(0, 2);
        else words[1] = words[1].substring(0, 1);
        this.day = Integer.parseInt(words[1]);

        this.year = Integer.parseInt(words[2]);
    }
    public void print(){
        System.out.println("Ngay " + this.day + " thang " + this.month + " nam " + this.year);
    }
}